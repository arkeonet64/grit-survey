﻿using Microsoft.VisualBasic.Devices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Grit_Survey_Server
{
    public static class ServerCommand
    {
        public static void Parse(String commandString)
        {
            switch (commandString)
            {
                case "":
                    break;
                case "exit":
                    Program.Exiting = true;
                    break;
                case "start_client":
                    if (Program.clientThread == null || //short-circuit the condition to prevent NullPointerException
                        !Program.clientThread.IsAlive)
                    {
                        Program.clientThread = new Thread(new ThreadStart(Program.startClient));
                        Program.clientThread.Start();
                    }
                    else
                    {
                        Console.WriteLine("A client is already running, there can only be one instance");
                    }
                    break;
                case "stop_client":
                    if (Program.clientThread != null &&
                        Program.clientThread.IsAlive)
                    {
                        Program.clientThread.Abort();
                    } else
                    {
                        Console.WriteLine("There is no client currently running");
                    }
                    break;
                case "info":
                    ComputerInfo computerInfo = new ComputerInfo();
                    Console.WriteLine("System Information");
                    Console.WriteLine("~~~~~~~~~~~~~~~~~~~~~~~~~");
                    Console.WriteLine("Available RAM: {0}", computerInfo.AvailablePhysicalMemory);
                    Console.WriteLine("OS Version: {0}", computerInfo.OSFullName);
                    Console.WriteLine("Arch: {0}", Environment.Is64BitProcess ? "x64" : "x86");
                    Console.WriteLine("Network IP/Hostname: {0}:{1}", Properties.Settings.Default.HOST_ADDRESS, Properties.Settings.Default.SERVER_PORT);
                    break;
                case "save":
                    Program.package.Save();
                    break;
                case "help":
                    Console.WriteLine("\n{0,-18} {1}", "Command", "Description");
                    Console.WriteLine("{0,-18} {1}\n", "~~~~~~~~~~~~~", "~~~~~~~~~~~~~~");
                    Console.WriteLine("{0,-18} {1}", "help", "Displays this message");
                    Console.WriteLine("{0,-18} {1}", "exit", "Exits the server");
                    Console.WriteLine("{0,-18} {1}", "info", "Shows information about the server");
                    Console.WriteLine("{0,-18} {1}", "start_client", "Starts a Grit Survey Client");
                    Console.WriteLine("{0,-18} {1}", "stop_client", "Stops a Grit Survey Client");
                    Console.WriteLine("{0,-18} {1}", "save", "Saves Excel spread");
                    break;
                default:
                    Console.WriteLine("Invalid command: {0}", commandString);
                    break;
            }
        }
    }
}
