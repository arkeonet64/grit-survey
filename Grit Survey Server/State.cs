﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Grit_Survey_Server
{
    class State
    {
        public Socket workSocket = null;
        public const int bufferSize = 70000;
        public Byte[] buffer = new byte[bufferSize];
        public List<Byte> clientBytes = new List<Byte>();
    }
}
