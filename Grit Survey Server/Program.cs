﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using Grit_Survey_Client;
using System.Data;
using OfficeOpenXml;
using System.Windows.Forms;
using System.Web.UI;
using System.Web.Script.Serialization;
using System.Diagnostics;
using System.Runtime.Serialization.Formatters.Soap;
using System.Runtime.Serialization.Json;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Grit_Survey_Server
{
    class Program
    {
        private static JavaScriptSerializer serializer = new JavaScriptSerializer();
        private static int row = 1;
        private static List<Object> flattenedUserChoices;
        private static readonly string FILE_NAME = "GRIT_SURVEY.xlsx";
        public static ExcelPackage package;
        private static ExcelWorksheet worksheet;
        private static TcpListener listener;


        public static WelcomeForm welcomeForm;
        public static Thread clientThread;
        public static Thread listenThread;
        private static Survey survey;


        public static bool Exiting { get; set; }

        static void Main(string[] args)
        {


            Console.Title = "Grit Survey: Server";
            Console.WriteLine("Please wait, Grit Survey: Server is initializing...");
            initialize();
            Console.WriteLine("Grit Survey: Server initialization complete");
            Console.WriteLine("Type 'help' to see a list of commands");

            listenThread = new Thread(new ThreadStart(listen));
            listenThread.Start();

            while (!Exiting)
            {
                Console.Write("|>> ");
                String input = Console.ReadLine().ToLower();
                ServerCommand.Parse(input);
            }
            listener.Stop();
            listenThread.Join();
            if (clientThread != null &&
                clientThread.IsAlive)
            {
                welcomeForm.Invoke((MethodInvoker)delegate ()
                {
                    welcomeForm.Close();
                });
                clientThread.Join();
            }
            package.Save();

            Console.CursorLeft = 0;
            Console.WriteLine("Grit Survey: Server shutdown complete");

            for (int i = 5; i >= 1; i--)
            {
                Console.Write("Exiting in {0}s", i);
                Console.CursorLeft = 0;
                Thread.Sleep(TimeSpan.FromSeconds(1));
            }
            return;
        }

        public static void startClient()
        {
            Application.EnableVisualStyles();
            Application.Run(welcomeForm = new WelcomeForm());
        }

        private static void listen()
        {
            try
            {
                //---listen at the specified IP and port no.---
                listener = new TcpListener(IPAddress.Any, Properties.Settings.Default.SERVER_PORT);
                Console.CursorLeft = 0;
                Console.WriteLine("Listening...");
                Console.Write("|>> ");
                listener.Start();

                while (!Exiting)
                {

                    //---incoming client connected---
                    TcpClient client = listener.AcceptTcpClient();
                    Console.CursorLeft = 0;
                    Console.WriteLine("Client Connected!");
                    Console.Write("|>> ");
                    List<byte> bytes = new List<byte>();

                    Thread.Sleep(TimeSpan.FromMilliseconds(3000));
                    //---get the incoming data through a network stream---
                    NetworkStream nwStream = client.GetStream();

                    Survey obj;

                    // Server Reply
                    if (nwStream.CanRead)
                    {
                        // Buffer to store the response bytes.
                        byte[] readBuffer = new byte[4096];

                        using (var writer = new MemoryStream())
                        {
                            while (true)
                            {
                                int numberOfBytesRead = nwStream.Read(readBuffer, 0, readBuffer.Length);
                                byte[] arr = new byte[numberOfBytesRead];
                                if (numberOfBytesRead <= 0)
                                {
                                    break;
                                }
                                Array.Copy(readBuffer, 0, arr, 0, numberOfBytesRead);
                                bytes.AddRange(arr);
                                Array.Clear(readBuffer, 0, readBuffer.Length);
                            }

                            Console.CursorLeft = 0;
                            string s = Encoding.UTF8.GetString(bytes.ToArray());
                            obj = JsonConvert.DeserializeObject<Survey>(s);
                            Console.WriteLine($"Deserialized client, got eGrit={obj.eGrit}, gritFirst={obj.gritFirst}");
                            addClientToData(obj);
                            Console.WriteLine("Received {0} bytes", bytes.Count);
                            Console.Write("|>> ");
                        }
                    }



                    client.Close();
                }

            }
            catch (Exception e)
            {
                Console.CursorLeft = 0;
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
                Console.Write("|>> ");
            }
        }

        private static void addClientToData(Survey survey)
        {
            //process this survey object into excel spreadsheet
            //flatten all dictionaries into a list
            Random randomGen = new Random(DateTime.Now.Millisecond);

            long userId = randomGen.Next(int.MaxValue).GetHashCode();

            while (containsUserId(userId))
            {
                userId = randomGen.Next(int.MaxValue).GetHashCode();
            }
            survey.userId = userId;

            flattenedUserChoices.Add((long)survey.userId);
            flattenedUserChoices.Add(survey.eGrit);
            flattenedUserChoices.Add(survey.gritFirst);
            foreach (var item in survey.sectionGrit)
            {
                flattenedUserChoices.Add(int.Parse(item.Value.getUserAnswer()));
                flattenedUserChoices.Add(item.Value.getQuestionAnswerText());
            }
            foreach (var item in survey.sectionWord)
            {
                flattenedUserChoices.Add(int.Parse(item.Value.getUserAnswer()));
                flattenedUserChoices.Add(item.Value.getQuestionTime());
            }
            foreach (var item in survey.sectionNumber)
            {
                flattenedUserChoices.Add(int.Parse(item.Value.getUserAnswer()));
                flattenedUserChoices.Add(item.Value.getQuestionTime());
            }
            foreach (var item in survey.sectionImage)
            {
                flattenedUserChoices.Add(int.Parse(item.Value.getUserAnswer()));
                flattenedUserChoices.Add(item.Value.getQuestionTime());
            }
            
            try
            {

                flattenedUserChoices.Add(survey.sectionDemographic["0"].getQuestionAnswerText());
                flattenedUserChoices.Add(survey.sectionDemographic["1"].getQuestionAnswerText());
                flattenedUserChoices.Add(survey.sectionDemographic["2"].getQuestionAnswerText());
                flattenedUserChoices.Add(int.Parse(survey.sectionDemographic["3"].getQuestionAnswerText()));
                flattenedUserChoices.Add(survey.sectionDemographic["4"].getQuestionAnswerText());
                flattenedUserChoices.Add(int.Parse(survey.sectionDemographic["5"].getQuestionAnswerText()));
                flattenedUserChoices.Add(float.Parse(survey.sectionDemographic["6"].getQuestionAnswerText()));
                flattenedUserChoices.Add(survey.sectionDemographic["7"].getQuestionAnswerText());
            }
            catch (Exception)
            {
                flattenedUserChoices.Add("USER_QUIT");
                flattenedUserChoices.Add("USER_QUIT");
                flattenedUserChoices.Add("USER_QUIT");
                flattenedUserChoices.Add("USER_QUIT");
                flattenedUserChoices.Add("USER_QUIT");
                flattenedUserChoices.Add("USER_QUIT");
                flattenedUserChoices.Add("USER_QUIT");
            }

            row++;
            for (int i = 1; i <= flattenedUserChoices.Count; i++)
            {
                worksheet.Cells[row, i].Value = flattenedUserChoices[i - 1];
            }
            flattenedUserChoices.Clear();
            package.Save();
        }

        private static void initialize()
        {
            Exiting = false;
            welcomeForm = new WelcomeForm();

            Properties.Settings.Default.HOST_ADDRESS = IPAddress.Any.ToString();

            flattenedUserChoices = new List<Object>();

            FileInfo fInfo = new FileInfo(FILE_NAME);
            try
            {
                package = new ExcelPackage(fInfo);
                if (!fInfo.Exists)
                {
                    worksheet = package.Workbook.Worksheets.Add("Grit Survey");
                    int col = 1;
                    worksheet.Cells[row, col++].Value = "userID";
                    worksheet.Cells[row, col++].Value = "E-Grit";
                    worksheet.Cells[row, col++].Value = "GritFirst";

                    worksheet.Cells[row, col++].Value = "grit_q0";
                    worksheet.Cells[row, col++].Value = "grit_q0_text";

                    worksheet.Cells[row, col++].Value = "grit_q1";
                    worksheet.Cells[row, col++].Value = "grit_q1_text";

                    worksheet.Cells[row, col++].Value = "grit_q2";
                    worksheet.Cells[row, col++].Value = "grit_q2_text";

                    worksheet.Cells[row, col++].Value = "grit_q3";
                    worksheet.Cells[row, col++].Value = "grit_q3_text";

                    worksheet.Cells[row, col++].Value = "grit_q4";
                    worksheet.Cells[row, col++].Value = "grit_q4_text";

                    worksheet.Cells[row, col++].Value = "grit_q5";
                    worksheet.Cells[row, col++].Value = "grit_q5_text";

                    worksheet.Cells[row, col++].Value = "grit_q6";
                    worksheet.Cells[row, col++].Value = "grit_q6_text";

                    worksheet.Cells[row, col++].Value = "grit_q7";
                    worksheet.Cells[row, col++].Value = "grit_q7_text";

                    worksheet.Cells[row, col++].Value = "word_q0";
                    worksheet.Cells[row, col++].Value = "word_q0_time";
                    worksheet.Cells[row, col++].Value = "word_q1";
                    worksheet.Cells[row, col++].Value = "word_q1_time";
                    worksheet.Cells[row, col++].Value = "word_q2";
                    worksheet.Cells[row, col++].Value = "word_q2_time";
                    worksheet.Cells[row, col++].Value = "word_q3";
                    worksheet.Cells[row, col++].Value = "word_q3_time";
                    worksheet.Cells[row, col++].Value = "word_q4";
                    worksheet.Cells[row, col++].Value = "word_q4_time";
                    worksheet.Cells[row, col++].Value = "word_q5";
                    worksheet.Cells[row, col++].Value = "word_q5_time";
                    worksheet.Cells[row, col++].Value = "word_q6";
                    worksheet.Cells[row, col++].Value = "word_q6_time";
                    worksheet.Cells[row, col++].Value = "word_q7";
                    worksheet.Cells[row, col++].Value = "word_q7_time";

                    worksheet.Cells[row, col++].Value = "number_q0";
                    worksheet.Cells[row, col++].Value = "number_q0_time";
                    worksheet.Cells[row, col++].Value = "number_q1";
                    worksheet.Cells[row, col++].Value = "number_q1_time";
                    worksheet.Cells[row, col++].Value = "number_q2";
                    worksheet.Cells[row, col++].Value = "number_q2_time";
                    worksheet.Cells[row, col++].Value = "number_q3";
                    worksheet.Cells[row, col++].Value = "number_q3_time";
                    worksheet.Cells[row, col++].Value = "number_q4";
                    worksheet.Cells[row, col++].Value = "number_q4_time";
                    worksheet.Cells[row, col++].Value = "number_q5";
                    worksheet.Cells[row, col++].Value = "number_q5_time";
                    worksheet.Cells[row, col++].Value = "number_q6";
                    worksheet.Cells[row, col++].Value = "number_q6_time";
                    worksheet.Cells[row, col++].Value = "number_q7";
                    worksheet.Cells[row, col++].Value = "number_q7_time";

                    worksheet.Cells[row, col++].Value = "mental_q0";
                    worksheet.Cells[row, col++].Value = "mental_q0_time";
                    worksheet.Cells[row, col++].Value = "mental_q1";
                    worksheet.Cells[row, col++].Value = "mental_q1_time";
                    worksheet.Cells[row, col++].Value = "mental_q2";
                    worksheet.Cells[row, col++].Value = "mental_q2_time";
                    worksheet.Cells[row, col++].Value = "mental_q3";
                    worksheet.Cells[row, col++].Value = "mental_q3_time";
                    worksheet.Cells[row, col++].Value = "mental_q4";
                    worksheet.Cells[row, col++].Value = "mental_q4_time";
                    worksheet.Cells[row, col++].Value = "mental_q5";
                    worksheet.Cells[row, col++].Value = "mental_q5_time";
                    worksheet.Cells[row, col++].Value = "mental_q6";
                    worksheet.Cells[row, col++].Value = "mental_q6_time";
                    worksheet.Cells[row, col++].Value = "mental_q7";
                    worksheet.Cells[row, col++].Value = "mental_q7_time";

                    worksheet.Cells[row, col++].Value = "demo_gender";//gender
                    worksheet.Cells[row, col++].Value = "demo_class";//class
                    worksheet.Cells[row, col++].Value = "demo_race";//race
                    worksheet.Cells[row, col++].Value = "demo_age";//age
                    worksheet.Cells[row, col++].Value = "demo_test_type";//test type
                    worksheet.Cells[row, col++].Value = "demo_score";//act score
                    worksheet.Cells[row, col++].Value = "demo_gpa";//gpa
                    worksheet.Cells[row, col++].Value = "demo_choose_all";//choose all

                    for (int i = 1; i < col; i++)
                    {
                        worksheet.Column(i).Width = 16;
                    }

                    package.Save();
                }
                else
                {
                    worksheet = package.Workbook.Worksheets["Grit Survey"];

                    for (int i = 1; i < worksheet.Cells.Rows; i++)
                    {
                        if (worksheet.Cells[i, 1].Text == "")
                        {
                            row = i;
                            break;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Excel file in use! Please close the file to continue operation.", "Grit Survey Server");
            }
        }

        private static bool containsUserId(long userId)
        {
            try
            {
                for (int i = 2; i < worksheet.Cells.Rows; i++)
                {
                    if (worksheet.Cells[i, 1].Value != null)
                    {
                        if ((double)worksheet.Cells[i, 1].Value == userId)
                        {
                            return true;
                        }
                    }
                    else
                    {
                        return false;
                    }
                }
                return false;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
