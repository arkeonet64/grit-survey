# Grit Survey #

This is a project in collaboration with members from the Psychology Department at Austin Peay State University. The Grit Survey Development Committee members are [Natasha Godkin](mailto:godkin@etsu.edu), [Dr. Kevin Harris](mailto:harrisk@apsu.edu), [Dr. Jessica Hatz](mailto:hatzj@apsu.edu), and [Brian Allison](mailto:ballison7@my.apsu.edu).

The goal of the Grit Survey is to collect data from participants into an Excel Spreadsheet in an off-site location. Ideally, this would be on a server behind a locked door with limited access. The client-server relationship is set up with asyncronous threads to send/receive data as necessary. As far as the details of the survey, there are four sections. Grit (which alternates an Enhanced-Grit version via randomization per particpant), Number Problems, Word Problems, and Rotational Images. The sections are randomized in order, but the Grit Section comes before or after the others. The final screen users see is a Demographics Section which includes details about the participant, but does not ask for a name or any identifying information. The main designers of the survey are Natasha Godkin, who designed the survey on paper, and myself. I developed the software and extrapolated her data into a User-Interface.


## Project Specifications ##

* Development Platform: Visual Studio 2017
* Languages: C# and XML
* Version: 1.0.0
* Dependencies: [EPPlus](https://github.com/JanKallman/EPPlus)
* Project Structure:
	+ Grit Survey Client
	+ Grit Survey Server

## Contribution Guidelines ##

If you have any contributes to make, please submit them into a commit and I will review your changes/additions. From hence forth, the XML that is shipped with the software should need to be altered to add or change problems.	

## Contacts ##

* [Natasha Godkin (Student Intern @ ETSU)](mailto:godkin@etsu.edu)
	+ Role: Survey Designer
* [Dr. Kevin Harris](mailto:harrisk@apsu.edu)
	+ Role: Department Sponser & Development Member
* [Dr. Jessica Hatz](mailto:hatzj@apsu.edu)
	+ Role: Department Sponser & Development Member
* [Brian Allison (GRA @ APSU)](mailto:ballizon7@my.apsu.edu)
	+ Role: Department Graduate Research Assistant & Development Member
* [Nathaniel Martin](mailto:nmartin11@my.apsu.edu)
	+ Role: Software Developer