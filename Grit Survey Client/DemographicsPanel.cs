﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Grit_Survey_Client
{
    public partial class DemographicsPanel : SurveyPanel
    {
        public DemographicsPanel()
        {
            InitializeComponent();
            scoreTypeCombobox.SelectedIndex = 0;
        }

        private void ageTextBox_Validating(object sender, CancelEventArgs e)
        {
            SurveyForm surveyForm = (SurveyForm)Form.ActiveForm;
            surveyForm.ageTextBox_Validating(sender, e);
        }

        private void scoreTextBox_Validating(object sender, CancelEventArgs e)
        {
            SurveyForm surveyForm = (SurveyForm)Form.ActiveForm;
            surveyForm.scoreTextBox_Validating(sender, e);

        }
       
        private void GPAtextBox_Validating(object sender, CancelEventArgs e)
        {
            SurveyForm surveyForm = (SurveyForm)Form.ActiveForm;
            surveyForm.GPAtextBox_Validating(sender, e);
        }
    }
}
