﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Grit_Survey_Client
{
    [JsonObject(MemberSerialization.Fields)]
    public class Question
    {
        double questionTime;
        int numSkips;
        String questionId;
        String questionText;
        String userAnswer;
        [JsonIgnore]
        Dictionary<String, String> answers;
        String questionAnswerText;
        [JsonIgnore]
        QuestionType questionType;

        public int NumSkips { get => numSkips; set => numSkips = value; }

        public Dictionary<String, String> getQuestionAnswers()
        {
            return answers;
        }
        public void addQuestionAnswer(String id, String text)
        {
            answers.Add(id, text);
        }

        public void addQuestionTime(double time)
        {
            questionTime += time;
        }

        public double getQuestionTime()
        {
            return questionTime;
        }

        public void setUserAnswer(String answer)
        {
            userAnswer = answer;
        }

        public String getQuestionAnswerText()
        {
            return questionAnswerText;
        }

        public void setQuestionAnswerText(String text)
        {
            questionAnswerText = text;
        }

        public String getUserAnswer()
        {
            return userAnswer;
        }

        public String getQuestionId()
        {
            return questionId;
        }

        public String getQuestionText()
        {
            return questionText;
        }

        public QuestionType getQuestionType()
        {
            return questionType;
        }

        private Question()
        {
            this.answers = new Dictionary<String, String>();
            this.questionId = "-1";
            this.questionText = "N/A";
            this.questionTime = 0f;
            this.questionAnswerText = "";
            this.userAnswer = "-1";
            this.NumSkips = 0;
        }

        public class Builder
        {
            Question q;

            public Builder()
            {
                q = new Question();
            }

            public Builder setQuestionId(String qId)
            {
                q.questionId = qId;
                return this;
            }

            public Builder setQuestionType(QuestionType type)
            {
                q.questionType = type;
                return this;
            }

            public Builder setQuestionText(String text)
            {
                q.questionText = text;
                return this;
            }

            public Question build()
            {
                return q;
            }
        }
    }
}
