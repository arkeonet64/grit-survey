﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Grit_Survey_Client
{
    public partial class QuestionPanel : SurveyPanel
    {
        public QuestionPanel()
        {
            InitializeComponent();
        }

        private void answerChoice_Click(object sender, EventArgs e)
        {
            SurveyForm surveyForm = (SurveyForm) Form.ActiveForm;
            surveyForm.answerChoice_Click(sender, e);
        }
    }
}
