﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Security.Permissions;
using System.Text;
using System.Threading.Tasks;

namespace Grit_Survey_Client
{
    [Serializable]
    public class Section : Dictionary<String, Question>
    {
        public String Name;
        public int Id;

        public Section()
        {
        }

        public Section(string name, int id)
        {
            Name = name;
            Id = id;
        }

        public Section(SerializationInfo info, StreamingContext context) : base(info, context)
        {

        }
    }
}
