﻿namespace Grit_Survey_Client
{
    partial class DemographicsPanel
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.fieldsRequiredLabel = new System.Windows.Forms.Label();
            this.scoreTextBox = new System.Windows.Forms.TextBox();
            this.sexLabel = new System.Windows.Forms.Label();
            this.femaleButton = new System.Windows.Forms.RadioButton();
            this.maleButton = new System.Windows.Forms.RadioButton();
            this.classLabel = new System.Windows.Forms.Label();
            this.multiSelectLabel = new System.Windows.Forms.Label();
            this.checkedListBoxes = new System.Windows.Forms.CheckedListBox();
            this.raceCombo = new System.Windows.Forms.ComboBox();
            this.raceLabel = new System.Windows.Forms.Label();
            this.ageTextBox = new System.Windows.Forms.TextBox();
            this.ageLabel = new System.Windows.Forms.Label();
            this.classCombo = new System.Windows.Forms.ComboBox();
            this.scoreTypeCombobox = new System.Windows.Forms.ComboBox();
            this.GPALabel = new System.Windows.Forms.Label();
            this.GPAtextBox = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // fieldsRequiredLabel
            // 
            this.fieldsRequiredLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.fieldsRequiredLabel.AutoSize = true;
            this.fieldsRequiredLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fieldsRequiredLabel.Location = new System.Drawing.Point(109, 97);
            this.fieldsRequiredLabel.Name = "fieldsRequiredLabel";
            this.fieldsRequiredLabel.Size = new System.Drawing.Size(151, 13);
            this.fieldsRequiredLabel.TabIndex = 42;
            this.fieldsRequiredLabel.Text = "All Fields Below Required";
            // 
            // scoreTextBox
            // 
            this.scoreTextBox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.scoreTextBox.Location = new System.Drawing.Point(154, 218);
            this.scoreTextBox.Name = "scoreTextBox";
            this.scoreTextBox.Size = new System.Drawing.Size(170, 20);
            this.scoreTextBox.TabIndex = 41;
            this.scoreTextBox.Validating += new System.ComponentModel.CancelEventHandler(this.scoreTextBox_Validating);
            // 
            // sexLabel
            // 
            this.sexLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.sexLabel.Location = new System.Drawing.Point(105, 110);
            this.sexLabel.Name = "sexLabel";
            this.sexLabel.Size = new System.Drawing.Size(44, 23);
            this.sexLabel.TabIndex = 34;
            this.sexLabel.Text = "Sex: ";
            this.sexLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // femaleButton
            // 
            this.femaleButton.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.femaleButton.AutoSize = true;
            this.femaleButton.Location = new System.Drawing.Point(209, 113);
            this.femaleButton.Name = "femaleButton";
            this.femaleButton.Size = new System.Drawing.Size(59, 17);
            this.femaleButton.TabIndex = 31;
            this.femaleButton.TabStop = true;
            this.femaleButton.Text = "Female";
            this.femaleButton.UseVisualStyleBackColor = true;
            // 
            // maleButton
            // 
            this.maleButton.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.maleButton.AutoSize = true;
            this.maleButton.Location = new System.Drawing.Point(155, 113);
            this.maleButton.Name = "maleButton";
            this.maleButton.Size = new System.Drawing.Size(48, 17);
            this.maleButton.TabIndex = 30;
            this.maleButton.TabStop = true;
            this.maleButton.Text = "Male";
            this.maleButton.UseVisualStyleBackColor = true;
            // 
            // classLabel
            // 
            this.classLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.classLabel.Location = new System.Drawing.Point(105, 133);
            this.classLabel.Name = "classLabel";
            this.classLabel.Size = new System.Drawing.Size(44, 23);
            this.classLabel.TabIndex = 33;
            this.classLabel.Text = "Class: ";
            this.classLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // multiSelectLabel
            // 
            this.multiSelectLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.multiSelectLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.multiSelectLabel.Location = new System.Drawing.Point(351, 116);
            this.multiSelectLabel.Name = "multiSelectLabel";
            this.multiSelectLabel.Size = new System.Drawing.Size(185, 23);
            this.multiSelectLabel.TabIndex = 39;
            this.multiSelectLabel.Text = "Please select all that apply:";
            this.multiSelectLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // checkedListBoxes
            // 
            this.checkedListBoxes.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.checkedListBoxes.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.checkedListBoxes.CheckOnClick = true;
            this.checkedListBoxes.FormattingEnabled = true;
            this.checkedListBoxes.Items.AddRange(new object[] {
            "Delayed enrollment",
            "Attends part-time for at-least part of the academic year",
            "Works full-time while enrolled",
            "Considered financial independent for purposes to determine financial aid",
            "Have dependents other than spouse",
            "Is a single parent",
            "Does not have a high school diploma",
            "1st generation college student"});
            this.checkedListBoxes.Location = new System.Drawing.Point(351, 142);
            this.checkedListBoxes.Name = "checkedListBoxes";
            this.checkedListBoxes.Size = new System.Drawing.Size(420, 135);
            this.checkedListBoxes.TabIndex = 28;
            // 
            // raceCombo
            // 
            this.raceCombo.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.raceCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.raceCombo.FormattingEnabled = true;
            this.raceCombo.Items.AddRange(new object[] {
            "White",
            "Hispanic/Latino",
            "Black/African-America",
            "Native-American/American-Indian",
            "Asian/Pacific Islander",
            "Other"});
            this.raceCombo.Location = new System.Drawing.Point(155, 191);
            this.raceCombo.Name = "raceCombo";
            this.raceCombo.Size = new System.Drawing.Size(170, 21);
            this.raceCombo.TabIndex = 38;
            // 
            // raceLabel
            // 
            this.raceLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.raceLabel.Location = new System.Drawing.Point(108, 186);
            this.raceLabel.Name = "raceLabel";
            this.raceLabel.Size = new System.Drawing.Size(41, 23);
            this.raceLabel.TabIndex = 37;
            this.raceLabel.Text = "Race:";
            this.raceLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ageTextBox
            // 
            this.ageTextBox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.ageTextBox.Location = new System.Drawing.Point(155, 163);
            this.ageTextBox.Name = "ageTextBox";
            this.ageTextBox.Size = new System.Drawing.Size(169, 20);
            this.ageTextBox.TabIndex = 36;
            this.ageTextBox.Validating += new System.ComponentModel.CancelEventHandler(this.ageTextBox_Validating);
            // 
            // ageLabel
            // 
            this.ageLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.ageLabel.Location = new System.Drawing.Point(108, 160);
            this.ageLabel.Name = "ageLabel";
            this.ageLabel.Size = new System.Drawing.Size(41, 23);
            this.ageLabel.TabIndex = 35;
            this.ageLabel.Text = "Age:";
            this.ageLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // classCombo
            // 
            this.classCombo.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.classCombo.BackColor = System.Drawing.Color.White;
            this.classCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.classCombo.FormattingEnabled = true;
            this.classCombo.Items.AddRange(new object[] {
            "Freshman",
            "Sophomore",
            "Junior",
            "Senior"});
            this.classCombo.Location = new System.Drawing.Point(154, 136);
            this.classCombo.Name = "classCombo";
            this.classCombo.Size = new System.Drawing.Size(170, 21);
            this.classCombo.TabIndex = 32;
            // 
            // scoreTypeCombobox
            // 
            this.scoreTypeCombobox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.scoreTypeCombobox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.scoreTypeCombobox.FormattingEnabled = true;
            this.scoreTypeCombobox.Items.AddRange(new object[] {
            "ACT Score",
            "SAT Score",
            "Compass Score",
            "ASVAB"});
            this.scoreTypeCombobox.Location = new System.Drawing.Point(27, 218);
            this.scoreTypeCombobox.Name = "scoreTypeCombobox";
            this.scoreTypeCombobox.Size = new System.Drawing.Size(121, 21);
            this.scoreTypeCombobox.TabIndex = 43;
            // 
            // GPALabel
            // 
            this.GPALabel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.GPALabel.AutoSize = true;
            this.GPALabel.Location = new System.Drawing.Point(112, 246);
            this.GPALabel.Name = "GPALabel";
            this.GPALabel.Size = new System.Drawing.Size(32, 13);
            this.GPALabel.TabIndex = 44;
            this.GPALabel.Text = "GPA:";
            // 
            // GPAtextBox
            // 
            this.GPAtextBox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.GPAtextBox.Location = new System.Drawing.Point(155, 246);
            this.GPAtextBox.Name = "GPAtextBox";
            this.GPAtextBox.Size = new System.Drawing.Size(170, 20);
            this.GPAtextBox.TabIndex = 45;
            this.GPAtextBox.Validating += new System.ComponentModel.CancelEventHandler(this.GPAtextBox_Validating);
            // 
            // DemographicsPanel
            // 
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.GPAtextBox);
            this.Controls.Add(this.GPALabel);
            this.Controls.Add(this.scoreTypeCombobox);
            this.Controls.Add(this.fieldsRequiredLabel);
            this.Controls.Add(this.scoreTextBox);
            this.Controls.Add(this.sexLabel);
            this.Controls.Add(this.femaleButton);
            this.Controls.Add(this.maleButton);
            this.Controls.Add(this.classLabel);
            this.Controls.Add(this.multiSelectLabel);
            this.Controls.Add(this.checkedListBoxes);
            this.Controls.Add(this.raceCombo);
            this.Controls.Add(this.raceLabel);
            this.Controls.Add(this.ageTextBox);
            this.Controls.Add(this.ageLabel);
            this.Controls.Add(this.classCombo);
            this.Name = "DemographicsPanel";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.Label fieldsRequiredLabel;
        public System.Windows.Forms.TextBox scoreTextBox;
        public System.Windows.Forms.Label sexLabel;
        public System.Windows.Forms.RadioButton femaleButton;
        public System.Windows.Forms.RadioButton maleButton;
        public System.Windows.Forms.Label classLabel;
        public System.Windows.Forms.Label multiSelectLabel;
        public System.Windows.Forms.CheckedListBox checkedListBoxes;
        public System.Windows.Forms.ComboBox raceCombo;
        public System.Windows.Forms.Label raceLabel;
        public System.Windows.Forms.TextBox ageTextBox;
        public System.Windows.Forms.Label ageLabel;
        public System.Windows.Forms.ComboBox classCombo;
        public System.Windows.Forms.ComboBox scoreTypeCombobox;
        private System.Windows.Forms.Label GPALabel;
        public System.Windows.Forms.TextBox GPAtextBox;
    }
}
