﻿namespace Grit_Survey_Client
{
    partial class SkipModal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SkipNowButton = new System.Windows.Forms.Button();
            this.SkipForeverButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // SkipNowButton
            // 
            this.SkipNowButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.SkipNowButton.AutoSize = true;
            this.SkipNowButton.Location = new System.Drawing.Point(12, 116);
            this.SkipNowButton.Name = "SkipNowButton";
            this.SkipNowButton.Size = new System.Drawing.Size(75, 23);
            this.SkipNowButton.TabIndex = 0;
            this.SkipNowButton.Text = "Skip Now";
            this.SkipNowButton.UseVisualStyleBackColor = true;
            this.SkipNowButton.Click += new System.EventHandler(this.SkipNowButton_Click);
            // 
            // SkipForeverButton
            // 
            this.SkipForeverButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.SkipForeverButton.AutoSize = true;
            this.SkipForeverButton.Location = new System.Drawing.Point(325, 116);
            this.SkipForeverButton.Name = "SkipForeverButton";
            this.SkipForeverButton.Size = new System.Drawing.Size(77, 23);
            this.SkipForeverButton.TabIndex = 1;
            this.SkipForeverButton.Text = "Skip Forever";
            this.SkipForeverButton.UseVisualStyleBackColor = true;
            this.SkipForeverButton.Click += new System.EventHandler(this.SkipForeverButton_Click);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(389, 39);
            this.label1.TabIndex = 2;
            this.label1.Text = "Skip Now - Will allow you to come back to the question later.";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(13, 61);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(356, 30);
            this.label2.TabIndex = 3;
            this.label2.Text = "Skip Forever - Will NOT allow you to come back to the question, and will be left " +
    "blank on submission.";
            // 
            // SkipModal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(414, 151);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.SkipForeverButton);
            this.Controls.Add(this.SkipNowButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "SkipModal";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Skip Question?";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button SkipNowButton;
        private System.Windows.Forms.Button SkipForeverButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
    }
}