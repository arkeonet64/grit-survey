﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Grit_Survey_Client
{
    public partial class SkipModal : Form
    {
        public const int MAX_SKIPS = 3;
        public SkipModal()
        {
            InitializeComponent();
        }
        public SkipModal(int numSkips)
        {
            InitializeComponent();
            if (numSkips == MAX_SKIPS)
            {
                label1.Text = "You are about to exceed the maximum number of skips allowed. If you choose to skip this question, it will be submitted as blank.";
                label1.TextAlign = ContentAlignment.TopLeft;
                label2.Visible = false;
                SkipNowButton.Visible = false;
                SkipForeverButton.Dock = DockStyle.Bottom;
            }
            else if (numSkips>MAX_SKIPS)
            {
                label1.Text = "There are no more questions left in rotation. If you choose to skip this question, it will be submitted as blank.";
                label1.TextAlign = ContentAlignment.TopLeft;
                label2.Visible = false;
                SkipNowButton.Visible = false;
                SkipForeverButton.Dock = DockStyle.Bottom;
            }
        }
        private void SkipNowButton_Click(object sender, EventArgs e)
        {
            //Set the Form's Dialog Result to Yes - depicting the user will return to the question later
            DialogResult = DialogResult.Yes;
        }

        private void SkipForeverButton_Click(object sender, EventArgs e)
        {
            //Set the Form's Dialog Result to No - depicting the user will never return to the question
            DialogResult = DialogResult.No;
        }
    }
}
