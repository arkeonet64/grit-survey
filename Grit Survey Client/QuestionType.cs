﻿namespace Grit_Survey_Client
{
    public enum QuestionType
    {
        image,
        grit,
        word,
        number,
        demographic
    }
}