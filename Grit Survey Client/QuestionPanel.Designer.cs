﻿namespace Grit_Survey_Client
{
    partial class QuestionPanel
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textEntryRequiredLabel = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.enteredTextBox = new System.Windows.Forms.TextBox();
            this.answerChoice4 = new System.Windows.Forms.RadioButton();
            this.answerChoice0 = new System.Windows.Forms.RadioButton();
            this.answerChoice3 = new System.Windows.Forms.RadioButton();
            this.answerChoice1 = new System.Windows.Forms.RadioButton();
            this.answerChoice2 = new System.Windows.Forms.RadioButton();
            this.questionTextBox = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // textEntryRequiredLabel
            // 
            this.textEntryRequiredLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.textEntryRequiredLabel.AutoSize = true;
            this.textEntryRequiredLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textEntryRequiredLabel.Location = new System.Drawing.Point(8, 369);
            this.textEntryRequiredLabel.Name = "textEntryRequiredLabel";
            this.textEntryRequiredLabel.Size = new System.Drawing.Size(120, 13);
            this.textEntryRequiredLabel.TabIndex = 35;
            this.textEntryRequiredLabel.Text = "Text Entry Required";
            this.textEntryRequiredLabel.Visible = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox1.Location = new System.Drawing.Point(352, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(144, 140);
            this.pictureBox1.TabIndex = 34;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Visible = false;
            // 
            // enteredTextBox
            // 
            this.enteredTextBox.AcceptsReturn = true;
            this.enteredTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.enteredTextBox.Enabled = false;
            this.enteredTextBox.Location = new System.Drawing.Point(2, 385);
            this.enteredTextBox.Multiline = true;
            this.enteredTextBox.Name = "enteredTextBox";
            this.enteredTextBox.Size = new System.Drawing.Size(845, 103);
            this.enteredTextBox.TabIndex = 33;
            this.enteredTextBox.Visible = false;
            // 
            // answerChoice4
            // 
            this.answerChoice4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.answerChoice4.AutoCheck = false;
            this.answerChoice4.Enabled = false;
            this.answerChoice4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.answerChoice4.Location = new System.Drawing.Point(350, 320);
            this.answerChoice4.Margin = new System.Windows.Forms.Padding(18, 3, 3, 14);
            this.answerChoice4.Name = "answerChoice4";
            this.answerChoice4.Size = new System.Drawing.Size(152, 46);
            this.answerChoice4.TabIndex = 32;
            this.answerChoice4.UseVisualStyleBackColor = true;
            this.answerChoice4.Click += new System.EventHandler(this.answerChoice_Click);
            // 
            // answerChoice0
            // 
            this.answerChoice0.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.answerChoice0.AutoCheck = false;
            this.answerChoice0.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.answerChoice0.Enabled = false;
            this.answerChoice0.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.answerChoice0.Location = new System.Drawing.Point(91, 169);
            this.answerChoice0.Margin = new System.Windows.Forms.Padding(18, 3, 3, 14);
            this.answerChoice0.Name = "answerChoice0";
            this.answerChoice0.Size = new System.Drawing.Size(152, 134);
            this.answerChoice0.TabIndex = 28;
            this.answerChoice0.UseVisualStyleBackColor = true;
            this.answerChoice0.Click += new System.EventHandler(this.answerChoice_Click);
            // 
            // answerChoice3
            // 
            this.answerChoice3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.answerChoice3.AutoCheck = false;
            this.answerChoice3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.answerChoice3.Enabled = false;
            this.answerChoice3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.answerChoice3.Location = new System.Drawing.Point(610, 169);
            this.answerChoice3.Margin = new System.Windows.Forms.Padding(18, 3, 3, 14);
            this.answerChoice3.Name = "answerChoice3";
            this.answerChoice3.Size = new System.Drawing.Size(152, 134);
            this.answerChoice3.TabIndex = 31;
            this.answerChoice3.UseVisualStyleBackColor = true;
            this.answerChoice3.Click += new System.EventHandler(this.answerChoice_Click);
            // 
            // answerChoice1
            // 
            this.answerChoice1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.answerChoice1.AutoCheck = false;
            this.answerChoice1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.answerChoice1.Enabled = false;
            this.answerChoice1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.answerChoice1.Location = new System.Drawing.Point(264, 169);
            this.answerChoice1.Margin = new System.Windows.Forms.Padding(18, 3, 3, 14);
            this.answerChoice1.Name = "answerChoice1";
            this.answerChoice1.Size = new System.Drawing.Size(152, 134);
            this.answerChoice1.TabIndex = 29;
            this.answerChoice1.UseVisualStyleBackColor = true;
            this.answerChoice1.Click += new System.EventHandler(this.answerChoice_Click);
            // 
            // answerChoice2
            // 
            this.answerChoice2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.answerChoice2.AutoCheck = false;
            this.answerChoice2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.answerChoice2.Enabled = false;
            this.answerChoice2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.answerChoice2.Location = new System.Drawing.Point(437, 169);
            this.answerChoice2.Margin = new System.Windows.Forms.Padding(18, 3, 3, 14);
            this.answerChoice2.Name = "answerChoice2";
            this.answerChoice2.Size = new System.Drawing.Size(152, 134);
            this.answerChoice2.TabIndex = 30;
            this.answerChoice2.UseVisualStyleBackColor = true;
            this.answerChoice2.Click += new System.EventHandler(this.answerChoice_Click);
            // 
            // questionTextBox
            // 
            this.questionTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.questionTextBox.BackColor = System.Drawing.Color.White;
            this.questionTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.questionTextBox.Cursor = System.Windows.Forms.Cursors.Default;
            this.questionTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.questionTextBox.Location = new System.Drawing.Point(0, 0);
            this.questionTextBox.Margin = new System.Windows.Forms.Padding(3, 3, 3, 40);
            this.questionTextBox.Multiline = true;
            this.questionTextBox.Name = "questionTextBox";
            this.questionTextBox.ReadOnly = true;
            this.questionTextBox.Size = new System.Drawing.Size(845, 242);
            this.questionTextBox.TabIndex = 0;
            // 
            // QuestionPanel
            // 
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.questionTextBox);
            this.Controls.Add(this.textEntryRequiredLabel);
            this.Controls.Add(this.enteredTextBox);
            this.Controls.Add(this.answerChoice4);
            this.Controls.Add(this.answerChoice0);
            this.Controls.Add(this.answerChoice3);
            this.Controls.Add(this.answerChoice1);
            this.Controls.Add(this.answerChoice2);
            this.Name = "QuestionPanel";
            this.Size = new System.Drawing.Size(848, 491);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.Label textEntryRequiredLabel;
        public System.Windows.Forms.PictureBox pictureBox1;
        public System.Windows.Forms.TextBox enteredTextBox;
        public System.Windows.Forms.RadioButton answerChoice4;
        public System.Windows.Forms.RadioButton answerChoice0;
        public System.Windows.Forms.RadioButton answerChoice3;
        public System.Windows.Forms.RadioButton answerChoice1;
        public System.Windows.Forms.RadioButton answerChoice2;
        public System.Windows.Forms.TextBox questionTextBox;
    }
}
