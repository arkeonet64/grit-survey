﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace Grit_Survey_Client
{
    [JsonObject(MemberSerialization.Fields)]
    public class Survey
    {
        public long userId;
        public bool eGrit;
        public bool gritFirst;
        public Section sectionDemographic;
        public Section sectionGrit;
        public Section sectionNumber;
        public Section sectionImage;
        public Section sectionWord;

        public Survey(Section Demographic,
        Section Grit,
        Section Number,
        Section Image,
        Section Word)
        {
            sectionDemographic = Demographic;
            sectionGrit = Grit;
            sectionNumber = Number;
            sectionImage = Image;
            sectionWord = Word;
            userId = int.MaxValue;
        }

        public Survey()
        {

        }
    }
}
