﻿using System;
using System.Diagnostics;
using System.IO;
using System.Xml;
using System.Xml.Schema;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.Net.Sockets;
using System.Net;
using System.Runtime.Serialization.Json;
using System.Runtime.Serialization.Formatters.Binary;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Grit_Survey_Client
{
    public partial class SurveyForm : Form
    {
        Stopwatch surveyStopwatch;
        Stopwatch questionStopwatch;
        int questionNumber;
        int sectionId;
        Random randomGen;
        Queue<Section> sectionOrderQueue;
        Survey survey;
        Dictionary<int, String> instructions;
        Section sectionDemographic;
        Section sectionGrit;
        Section sectionNumber;
        Section sectionImage;
        Section sectionWord;

        Section currentSection;
        Question currentQuestion;

        Queue<Question> skippedQuestions;

        private static ManualResetEvent connectionComplete;

        private static ManualResetEvent sendComplete;

        DemographicsPanel demographicsPanel;
        QuestionPanel questionPanel;

        public SurveyForm()
        {
            InitializeComponent();

            survey = new Survey();

            questionPanel = new QuestionPanel();
            demographicsPanel = new DemographicsPanel();

            skippedQuestions = new Queue<Question>();

            connectionComplete = new ManualResetEvent(false);
            sendComplete = new ManualResetEvent(false);

            surveyStopwatch = new Stopwatch();
            questionStopwatch = new Stopwatch();
            KeyPreview = true;
            KeyDown += GritSurveyQuestionnaire_KeyDown;
            randomGen = new Random(DateTime.Now.Millisecond);
            instructions = new Dictionary<int, String>();

            survey.userId = long.MaxValue;

            List<Section> sectionOrderList = new List<Section>();
            sectionDemographic = new Section("Demographic", 4);
            sectionGrit = new Section("Grit", 0);
            sectionNumber = new Section("Number", 2);
            sectionImage = new Section("MentalRotations", 3);
            sectionWord = new Section("Word", 1);

            sectionOrderList.Add(sectionNumber);
            sectionOrderList.Add(sectionImage);
            sectionOrderList.Add(sectionWord);

            for (int i = 0; i < sectionOrderList.Count; i++)
            {
                Section temp = sectionOrderList[i];
                int randomIndex = randomGen.Next(i, sectionOrderList.Count);
                sectionOrderList[i] = sectionOrderList[randomIndex];
                sectionOrderList[randomIndex] = temp;
            }

            if (randomGen.Next(0, 100) >= 50)
            {
                sectionOrderList.Insert(0, sectionGrit);
            }
            else
            {
                sectionOrderList.Add(sectionGrit);
            }

            //if random number is greater than or equal to 50, make it an eGritSurvey
            if (randomGen.Next(0, 100) >= 50)
            {
                survey.eGrit = true;
            }
            else
            {
                survey.eGrit = false;
            }

            survey.gritFirst = sectionOrderList[0] == sectionGrit; //compare first item to see if it is the grit section


            sectionOrderList.Add(sectionDemographic);
            questionNumber = -1;
            sectionOrderQueue = new Queue<Section>(sectionOrderList);
            importQuestionData();
        }
        
        private void GritSurveyQuestionnaire_Shown(object sender, EventArgs e)
        {
            //Start surveyTimer && questionTimer
            surveyStopwatch.Start();

            Controls.Add(questionPanel);

            questionPanel.Location = surveyPanel.Location;
            questionPanel.Anchor = AnchorStyles.Top & AnchorStyles.Bottom & AnchorStyles.Left & AnchorStyles.Right;
            questionPanel.Size = surveyPanel.Size;
            questionPanel.BringToFront();

            questionPanel.Show();
            changeSection();
        }

        private void importQuestionData()
        {
            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.Load("GritSurvey.xml");

            foreach (XmlNode node in xmlDocument.DocumentElement.ChildNodes)
            {
                switch (node.Name)
                {
                    case "Section":
                        String sectionName = node.Attributes["name"].InnerText;
                        instructions.Add(int.Parse(node.Attributes["id"].InnerText),
                            node.Attributes["instructions"].InnerText);

                        foreach (XmlNode questionNode in node.ChildNodes)
                        {
                            //Set Question Type
                            QuestionType type = QuestionType.grit;
                            if (sectionName == "Grit Survey")
                            {
                                type = QuestionType.grit;
                            }
                            else if (sectionName == "Word Problems")
                            {
                                type = QuestionType.word;
                            }
                            else if (sectionName == "Number Problems")
                            {
                                type = QuestionType.number;
                            }
                            else if (sectionName == "Mental Rotation Problems")
                            {
                                type = QuestionType.image;
                            }
                            else if (sectionName == "Demographic Survey")
                            {
                                type = QuestionType.demographic;
                            }

                            //Build Question Object
                            Question currentQuestion;

                            //Add Question Object to proper Section
                            switch (type)
                            {
                                case QuestionType.image:
                                    currentQuestion = new Question.Builder()
                                        .setQuestionId(questionNode.Attributes["id"].InnerText)
                                        .setQuestionText(questionNode.Attributes["source"].InnerText)
                                        .setQuestionType(type)
                                        .build();
                                    foreach (XmlNode answerNode in questionNode.ChildNodes)
                                    {
                                        currentQuestion.addQuestionAnswer(answerNode.Attributes["id"].InnerText,
                                            answerNode.Attributes["source"].InnerText);
                                    }
                                    sectionImage.Add(currentQuestion.getQuestionId(), currentQuestion);
                                    break;
                                case QuestionType.grit:
                                    currentQuestion = new Question.Builder()
                                         .setQuestionId(questionNode.Attributes["id"].InnerText)
                                         .setQuestionText(questionNode.Attributes["text"].InnerText)
                                         .setQuestionType(type)
                                         .build();
                                    sectionGrit.Add(currentQuestion.getQuestionId(), currentQuestion);

                                    break;
                                case QuestionType.word:
                                    currentQuestion = new Question.Builder()
                                         .setQuestionId(questionNode.Attributes["id"].InnerText)
                                         .setQuestionText(questionNode.Attributes["text"].InnerText)
                                         .setQuestionType(type)
                                         .build();
                                    foreach (XmlNode answerNode in questionNode.ChildNodes)
                                    {
                                        currentQuestion.addQuestionAnswer(answerNode.Attributes["id"].InnerText,
                                            answerNode.InnerText);
                                    }
                                    sectionWord.Add(currentQuestion.getQuestionId(), currentQuestion);
                                    break;
                                case QuestionType.number:
                                    currentQuestion = new Question.Builder()
                                         .setQuestionId(questionNode.Attributes["id"].InnerText)
                                         .setQuestionText(questionNode.Attributes["text"].InnerText)
                                         .setQuestionType(type)
                                         .build();
                                    foreach (XmlNode answerNode in questionNode.ChildNodes)
                                    {
                                        currentQuestion.addQuestionAnswer(answerNode.Attributes["id"].InnerText,
                                            answerNode.InnerText);
                                    }
                                    sectionNumber.Add(currentQuestion.getQuestionId(), currentQuestion);
                                    break;
                                default:
                                    break;
                            }
                        }
                        break;
                    default:
                        break;
                }
            }

        }

        private void nextButton_Click(object sender, EventArgs e)
        {
            if (Next_SkipButton.Text == "Submit")
            {
                if (!demographicsPanel.maleButton.Checked &&
                    !demographicsPanel.femaleButton.Checked)
                {
                    return;
                }
                if (demographicsPanel.classCombo.Text == "" ||
                    demographicsPanel.raceCombo.Text == "" ||
                    demographicsPanel.ageTextBox.Text == "" ||
                    demographicsPanel.scoreTextBox.Text == "" ||
                    demographicsPanel.GPAtextBox.Text == "")
                {
                    MessageBox.Show("You have incomplete items on this screen. Please complete them all before submitting.", "Grit Survey: Incomplete Items");
                    return;
                }
                if (ageErrorProvider.GetError(demographicsPanel.ageTextBox) != "" ||
                    actErrorProvider.GetError(demographicsPanel.scoreTextBox) != "" ||
                    gpaErrorProvider.GetError(demographicsPanel.GPAtextBox) != "")
                {
                    MessageBox.Show("There are problems with items on this screen. Please fix them before.", "Grit Survey: Invalid Data");
                    return;
                }
                surveyStopwatch.Stop();
                questionStopwatch.Stop();
                //Add demo data
                if (demographicsPanel.maleButton.Checked)
                {
                    Question q = new Question.Builder()
                    .setQuestionId("0")
                    .setQuestionType(QuestionType.demographic)
                    .build();
                    q.setQuestionAnswerText("male");
                    sectionDemographic.Add("0", q);
                }
                else if (demographicsPanel.femaleButton.Checked)
                {
                    Question q = new Question.Builder()
                    .setQuestionId("0")
                    .setQuestionType(QuestionType.demographic)
                    .build();
                    q.setQuestionAnswerText("female");
                    sectionDemographic.Add("0", q);
                }

                Question qClass = new Question.Builder()
                .setQuestionId("1")
                .setQuestionType(QuestionType.demographic)
                .build();
                qClass.setQuestionAnswerText(demographicsPanel.classCombo.Text);
                sectionDemographic.Add("1", qClass);

                Question qRace = new Question.Builder()
                .setQuestionId("2")
                .setQuestionType(QuestionType.demographic)
                .build();
                qRace.setQuestionAnswerText(demographicsPanel.raceCombo.Text);
                sectionDemographic.Add("2", qRace);

                Question qAge = new Question.Builder()
                .setQuestionId("3")
                .setQuestionType(QuestionType.demographic)
                .build();
                qAge.setQuestionAnswerText(demographicsPanel.ageTextBox.Text);
                sectionDemographic.Add("3", qAge);

                Question qScoreType = new Question.Builder()
                .setQuestionId("4")
                .setQuestionType(QuestionType.demographic)
                .build();
                qScoreType.setQuestionAnswerText(demographicsPanel.scoreTypeCombobox.Text);
                sectionDemographic.Add("4", qScoreType);

                Question qScore = new Question.Builder()
                .setQuestionId("5")
                .setQuestionType(QuestionType.demographic)
                .build();
                qScore.setQuestionAnswerText(demographicsPanel.scoreTextBox.Text);
                sectionDemographic.Add("5", qScore);

                Question qGPA = new Question.Builder()
                    .setQuestionId("6")
                    .setQuestionType(QuestionType.demographic)
                    .build();
                qGPA.setQuestionAnswerText(demographicsPanel.GPAtextBox.Text);
                sectionDemographic.Add("6", qGPA);

                //Convert CheckBoxes to String
                String checkedAnswers = "";

                foreach (String item in demographicsPanel.checkedListBoxes.CheckedItems)
                {
                    if (demographicsPanel.checkedListBoxes.CheckedItems.IndexOf(item) < demographicsPanel.checkedListBoxes.CheckedItems.Count - 1)
                    {
                        checkedAnswers += item + ", ";

                    }
                    else
                    {
                        checkedAnswers += item;
                    }
                }

                Question qChooseAll = new Question.Builder()
                .setQuestionId("7")
                .setQuestionType(QuestionType.demographic)
                .build();
                qChooseAll.setQuestionAnswerText(checkedAnswers);
                sectionDemographic.Add("7", qChooseAll);

                //Send data
                survey.sectionDemographic = sectionDemographic;
                survey.sectionGrit = sectionGrit;
                survey.sectionImage = sectionImage;
                survey.sectionNumber = sectionNumber;
                survey.sectionWord = sectionWord;
                sendSurvey();
                //Close form
                Close();
            }
            else
            {
                //if the first question is to be displayed, setup the form
                if (questionNumber == -1 && Next_SkipButton.Text == "Next")
                {
                    if (currentSection.Id != 0)
                    {
                        Next_SkipButton.Text = "Skip";
                    }
                    questionNumber++;
                    questionStopwatch.Restart();
                    displayQuestion(currentSection);

                    if (sectionId == 3 && questionNumber >= 0)
                    {
                        questionPanel.pictureBox1.Show();
                    }
                    else if (sectionId == 0 && survey.eGrit)
                    {
                        questionPanel.enteredTextBox.Enabled = true;
                    }

                    enableAnswerChoices();
                }
                else if (Next_SkipButton.Text == "Skip")
                {
                    DialogResult dr = DialogResult.None;
                    if (currentQuestion.NumSkips >= 1 && skippedQuestions.Count == 0)
                    {
                        dr = new SkipModal(SkipModal.MAX_SKIPS + 1).ShowDialog();
                    }
                    else if (currentQuestion.NumSkips >= 1 && skippedQuestions.Count > 0)
                    {
                        dr = new SkipModal(currentQuestion.NumSkips).ShowDialog();

                    }
                    else
                    {
                        dr = new SkipModal().ShowDialog();

                    }
                    switch (dr)
                    {
                        case DialogResult.None:
                            break;
                        case DialogResult.OK:
                            break;
                        case DialogResult.Cancel:
                            break;
                        case DialogResult.Abort:
                            break;
                        case DialogResult.Retry:
                            break;
                        case DialogResult.Ignore:
                            break;
                        case DialogResult.Yes:
                            //User wishes to come back to the question
                            currentQuestion.NumSkips++;
                            saveAnswer(currentQuestion);
                            skippedQuestions.Enqueue(currentSection[currentQuestion.getQuestionId()]);
                            questionStopwatch.Restart();
                            questionNumber++;

                            displayQuestion(currentSection);

                            Next_SkipButton.Text = "Skip";
                            break;
                        case DialogResult.No:
                            //User wishes to never come back to the question
                            currentQuestion.NumSkips++;
                            saveAnswer(currentQuestion);
                            questionStopwatch.Restart();

                            if (questionNumber >= currentSection.Count && skippedQuestions.Count == 0)
                            {
                                changeSection();
                                return;
                            }

                            questionNumber++;

                            displayQuestion(currentSection);

                            Next_SkipButton.Text = "Skip";
                            break;
                        default:
                            break;
                    }
                }
                //If user is selected a choice and wishes to continue, save the answer, and move on.
                else if (questionPanel.answerChoice0.Checked ||
                    questionPanel.answerChoice1.Checked ||
                    questionPanel.answerChoice2.Checked ||
                    questionPanel.answerChoice3.Checked ||
                    questionPanel.answerChoice4.Checked)
                {
                    if (questionPanel.enteredTextBox.Visible && sectionId == 0 && questionPanel.enteredTextBox.Text == "")
                    {
                        return;
                    }
                    saveAnswer(currentQuestion);
                    questionStopwatch.Restart();

                    questionNumber++;

                    clearAnswerChoices();

                    if (currentSection.Id != 0)
                    {
                        Next_SkipButton.Text = "Skip";
                    }
                    else if (currentSection.Id == 0 && survey.eGrit)
                    {
                        questionPanel.enteredTextBox.Text = "";
                    }

                    displayQuestion(currentSection);
                    if (sectionOrderQueue.Count == 0 && questionNumber == sectionDemographic.Count)
                    {
                        Next_SkipButton.Text = "Finish";
                    }
                }
            }
        }

        private void displayQuestion(Section section)
        {

            questionStopwatch.Restart();

            //End of section
            if (questionNumber >= currentSection.Count && skippedQuestions.Count == 0)
            {
                changeSection();
            }
            //End of section, but need to go through skipped ones
            else if (questionNumber >= currentSection.Count && skippedQuestions.Count > 0)
            {
                currentQuestion = skippedQuestions.Dequeue();
                switch (currentQuestion.getQuestionType())
                {
                    case QuestionType.image:
                        displayMentalRotations(currentQuestion);
                        break;
                    case QuestionType.word:
                        questionPanel.answerChoice0.Text = currentQuestion.getQuestionAnswers()["0"];
                        questionPanel.answerChoice1.Text = currentQuestion.getQuestionAnswers()["1"];
                        questionPanel.answerChoice2.Text = currentQuestion.getQuestionAnswers()["2"];
                        questionPanel.answerChoice3.Text = currentQuestion.getQuestionAnswers()["3"];
                        questionPanel.questionTextBox.Text = currentQuestion.getQuestionText();
                        break;
                    case QuestionType.number:
                        questionPanel.answerChoice0.Text = currentQuestion.getQuestionAnswers()["0"];
                        questionPanel.answerChoice1.Text = currentQuestion.getQuestionAnswers()["1"];
                        questionPanel.answerChoice2.Text = currentQuestion.getQuestionAnswers()["2"];
                        questionPanel.answerChoice3.Text = currentQuestion.getQuestionAnswers()["3"];
                        questionPanel.questionTextBox.Text = currentQuestion.getQuestionText();
                        break;
                    default:
                        break;
                }
            }
            else
            {
                currentQuestion = section[questionNumber.ToString()];
                switch (currentQuestion.getQuestionType())
                {
                    case QuestionType.image:
                        displayMentalRotations(null);
                        break;
                    case QuestionType.grit:
                        questionPanel.questionTextBox.Text = currentQuestion.getQuestionText();
                        break;
                    case QuestionType.word:
                        questionPanel.answerChoice0.Text = currentQuestion.getQuestionAnswers()["0"];
                        questionPanel.answerChoice1.Text = currentQuestion.getQuestionAnswers()["1"];
                        questionPanel.answerChoice2.Text = currentQuestion.getQuestionAnswers()["2"];
                        questionPanel.answerChoice3.Text = currentQuestion.getQuestionAnswers()["3"];
                        questionPanel.questionTextBox.Text = currentQuestion.getQuestionText();
                        break;
                    case QuestionType.number:
                        questionPanel.answerChoice0.Text = currentQuestion.getQuestionAnswers()["0"];
                        questionPanel.answerChoice1.Text = currentQuestion.getQuestionAnswers()["1"];
                        questionPanel.answerChoice2.Text = currentQuestion.getQuestionAnswers()["2"];
                        questionPanel.answerChoice3.Text = currentQuestion.getQuestionAnswers()["3"];
                        questionPanel.questionTextBox.Text = currentQuestion.getQuestionText();
                        break;
                    case QuestionType.demographic:
                        questionPanel.questionTextBox.Text = currentQuestion.getQuestionText();
                        break;
                    default:
                        break;
                }
            }

        }

        private void saveAnswer(Question _cQuestion)
        {
            if (questionPanel.answerChoice0.Checked)
            {
                _cQuestion.setUserAnswer("0");
            }
            else if (questionPanel.answerChoice1.Checked)
            {
                _cQuestion.setUserAnswer("1");
            }
            else if (questionPanel.answerChoice2.Checked)
            {
                _cQuestion.setUserAnswer("2");
            }
            else if (questionPanel.answerChoice3.Checked)
            {
                _cQuestion.setUserAnswer("3");
            }
            else if (questionPanel.answerChoice4.Checked)
            {
                _cQuestion.setUserAnswer("4");
            }

            if (sectionId == 0 && survey.eGrit)
            {
                _cQuestion.setQuestionAnswerText(questionPanel.enteredTextBox.Text);
            }

            if (sectionId != 0 && sectionId != 4)
            {
                _cQuestion.addQuestionTime(Double.Parse(questionStopwatch.Elapsed.TotalSeconds.ToString("0.00")));
            }

        }
        private void displayMentalRotations(Question question)
        {
            questionPanel.questionTextBox.Text = String.Empty;
            questionPanel.pictureBox1.Show();

            int _qNum = questionNumber;

            if (question != null)
            {
                _qNum = int.Parse(question.getQuestionId());
            }

            switch (_qNum)
            {
                case 0:
                    questionPanel.pictureBox1.BackgroundImage = Properties.Resources.question1;
                    questionPanel.answerChoice0.BackgroundImage = Properties.Resources.question1_answer0;
                    questionPanel.answerChoice1.BackgroundImage = Properties.Resources.question1_answer1;
                    questionPanel.answerChoice2.BackgroundImage = Properties.Resources.question1_answer2;
                    questionPanel.answerChoice3.BackgroundImage = Properties.Resources.question1_answer3;
                    break;
                case 1:
                    questionPanel.pictureBox1.BackgroundImage = Properties.Resources.question2;
                    questionPanel.answerChoice0.BackgroundImage = Properties.Resources.question2_answer0;
                    questionPanel.answerChoice1.BackgroundImage = Properties.Resources.question2_answer1;
                    questionPanel.answerChoice2.BackgroundImage = Properties.Resources.question2_answer2;
                    questionPanel.answerChoice3.BackgroundImage = Properties.Resources.question2_answer3;
                    break;
                case 2:
                    questionPanel.pictureBox1.BackgroundImage = Properties.Resources.question3;
                    questionPanel.answerChoice0.BackgroundImage = Properties.Resources.question3_answer0;
                    questionPanel.answerChoice1.BackgroundImage = Properties.Resources.question3_answer1;
                    questionPanel.answerChoice2.BackgroundImage = Properties.Resources.question3_answer2;
                    questionPanel.answerChoice3.BackgroundImage = Properties.Resources.question3_answer3;
                    break;
                case 3:
                    questionPanel.pictureBox1.BackgroundImage = Properties.Resources.question4;
                    questionPanel.answerChoice0.BackgroundImage = Properties.Resources.question4_answer0;
                    questionPanel.answerChoice1.BackgroundImage = Properties.Resources.question4_answer1;
                    questionPanel.answerChoice2.BackgroundImage = Properties.Resources.question4_answer2;
                    questionPanel.answerChoice3.BackgroundImage = Properties.Resources.question4_answer3;
                    break;
                case 4:
                    questionPanel.pictureBox1.BackgroundImage = Properties.Resources.question5;
                    questionPanel.answerChoice0.BackgroundImage = Properties.Resources.question5_answer0;
                    questionPanel.answerChoice1.BackgroundImage = Properties.Resources.question5_answer1;
                    questionPanel.answerChoice2.BackgroundImage = Properties.Resources.question5_answer2;
                    questionPanel.answerChoice3.BackgroundImage = Properties.Resources.question5_answer3;
                    break;
                case 5:
                    questionPanel.pictureBox1.BackgroundImage = Properties.Resources.question6;
                    questionPanel.answerChoice0.BackgroundImage = Properties.Resources.question6_answer0;
                    questionPanel.answerChoice1.BackgroundImage = Properties.Resources.question6_answer1;
                    questionPanel.answerChoice2.BackgroundImage = Properties.Resources.question6_answer2;
                    questionPanel.answerChoice3.BackgroundImage = Properties.Resources.question6_answer3;
                    break;
                case 6:
                    questionPanel.pictureBox1.BackgroundImage = Properties.Resources.question7;
                    questionPanel.answerChoice0.BackgroundImage = Properties.Resources.question7_answer0;
                    questionPanel.answerChoice1.BackgroundImage = Properties.Resources.question7_answer1;
                    questionPanel.answerChoice2.BackgroundImage = Properties.Resources.question7_answer2;
                    questionPanel.answerChoice3.BackgroundImage = Properties.Resources.question7_answer3;
                    break;
                case 7:
                    questionPanel.pictureBox1.BackgroundImage = Properties.Resources.question8;
                    questionPanel.answerChoice0.BackgroundImage = Properties.Resources.question8_answer0;
                    questionPanel.answerChoice1.BackgroundImage = Properties.Resources.question8_answer1;
                    questionPanel.answerChoice2.BackgroundImage = Properties.Resources.question8_answer2;
                    questionPanel.answerChoice3.BackgroundImage = Properties.Resources.question8_answer3;
                    break;

                default:
                    break;
            }
        }

        private void changeSection()
        {
            disableAnswerChoices();
            removeImagesFromAnswerChoices();
            Next_SkipButton.Text = "Next";

            questionNumber = -1;
            currentSection = sectionOrderQueue.Dequeue();
            sectionId = currentSection.Id;
            String questionText = String.Empty;
            String title = String.Empty;
            questionText = instructions[sectionId];

            if (sectionId != 0)
            {
                QuitSectionButton.Show();
            }
            else
            {
                QuitSectionButton.Hide();
            }

            questionPanel.questionTextBox.Font = new Font("Microsoft Sans Serif", 12, FontStyle.Regular);
            questionPanel.questionTextBox.ScrollBars = ScrollBars.None;

            questionPanel.questionTextBox.Text = questionText;
            switch (sectionId)
            {
                case 0:
                    title = "Grit Survey";
                    questionPanel.answerChoice0.Text = "Very much like me";
                    questionPanel.answerChoice1.Text = "Mostly like me";
                    questionPanel.answerChoice2.Text = "Somewhat like me";
                    questionPanel.answerChoice3.Text = "Not much like me";
                    questionPanel.answerChoice4.Text = "Not like me at all";
                    questionPanel.answerChoice4.Show();
                    questionPanel.pictureBox1.Hide();
                    removeImagesFromAnswerChoices();
                    if (survey.eGrit)
                    {
                        questionPanel.questionTextBox.Font = new Font("Microsoft Sans Serif", 10, FontStyle.Regular);
                        questionPanel.questionTextBox.ScrollBars = ScrollBars.Vertical;
                        questionPanel.textEntryRequiredLabel.Show();
                        questionPanel.enteredTextBox.Show();
                    }
                    else
                    {
                        questionPanel.questionTextBox.Text =
                            instructions[sectionId] = questionPanel.questionTextBox.Lines[0];
                    }
                    QuitSectionButton.Hide();
                    //questionPanel.questionTextBox.TextAlign = HorizontalAlignment.Center;
                    break;
                case 1:
                    title = "Word Problems";
                    resetAnswerChoices();
                    questionPanel.answerChoice4.Hide();
                    questionPanel.textEntryRequiredLabel.Hide();
                    questionPanel.pictureBox1.Hide();
                    removeImagesFromAnswerChoices();
                    questionPanel.textEntryRequiredLabel.Hide();
                    questionPanel.enteredTextBox.Hide();
                    questionPanel.questionTextBox.TextAlign = HorizontalAlignment.Left;
                    break;
                case 2:
                    title = "Number Problems";
                    resetAnswerChoices();
                    questionPanel.answerChoice4.Hide();
                    questionPanel.pictureBox1.Hide();
                    removeImagesFromAnswerChoices();
                    questionPanel.textEntryRequiredLabel.Hide();
                    questionPanel.enteredTextBox.Hide();
                    questionPanel.questionTextBox.TextAlign = HorizontalAlignment.Left;
                    break;
                case 3:
                    title = "Mental Rotation Problems";
                    resetAnswerChoices();
                    questionPanel.answerChoice4.Hide();
                    questionPanel.textEntryRequiredLabel.Hide();
                    questionPanel.enteredTextBox.Hide();
                    questionPanel.pictureBox1.BringToFront();
                    break;
                case 4:
                    Controls.Remove(questionPanel);
                    Controls.Add(demographicsPanel);

                    demographicsPanel.Location = surveyPanel.Location;
                    demographicsPanel.Anchor = AnchorStyles.Top & AnchorStyles.Bottom & AnchorStyles.Left & AnchorStyles.Right;
                    demographicsPanel.Size = surveyPanel.Size;
                    demographicsPanel.BringToFront();

                    demographicsPanel.Show();

                    title = "Demographic Survey";
                    questionPanel.answerChoice0.Hide();
                    questionPanel.answerChoice1.Hide();
                    questionPanel.answerChoice2.Hide();
                    questionPanel.answerChoice3.Hide();
                    questionPanel.answerChoice4.Hide();
                    questionPanel.pictureBox1.Hide();
                    questionPanel.textEntryRequiredLabel.Hide();
                    questionPanel.enteredTextBox.Hide();
                    QuitSectionButton.Hide();
                    QuitSectionTooltip.Active = false;
                    InstructionsButton.Hide();

                    questionPanel.questionTextBox.TextAlign = HorizontalAlignment.Left;
                    FinishTooltip.Active = true;
                    Next_SkipButton.Text = "Submit";
                    break;
                default:
                    break;
            }
            titleTextBox.Text = title;
        }

        private void disableAnswerChoices()
        {
            questionPanel.answerChoice0.Enabled = false;
            questionPanel.answerChoice1.Enabled = false;
            questionPanel.answerChoice2.Enabled = false;
            questionPanel.answerChoice3.Enabled = false;
            questionPanel.answerChoice4.Enabled = false;
        }
        private void enableAnswerChoices()
        {
            questionPanel.answerChoice0.Enabled = true;
            questionPanel.answerChoice1.Enabled = true;
            questionPanel.answerChoice2.Enabled = true;
            questionPanel.answerChoice3.Enabled = true;
            questionPanel.answerChoice4.Enabled = true;
        }

        private void resetAnswerChoices()
        {
            questionPanel.answerChoice0.Text = "";
            questionPanel.answerChoice1.Text = "";
            questionPanel.answerChoice2.Text = "";
            questionPanel.answerChoice3.Text = "";
            questionPanel.answerChoice4.Text = "";
        }

        private void removeImagesFromAnswerChoices()
        {
            questionPanel.answerChoice0.BackgroundImage = null;
            questionPanel.answerChoice1.BackgroundImage = null;
            questionPanel.answerChoice2.BackgroundImage = null;
            questionPanel.answerChoice3.BackgroundImage = null;
        }

        void GritSurveyQuestionnaire_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Alt && e.KeyCode == Keys.F4 ||
                e.Alt && e.KeyCode == Keys.Tab)
            {
                e.SuppressKeyPress = true;
            }
            else if (e.Control && e.Shift && e.KeyCode == Keys.D1)
            {
                DialogResult dialogResult = MessageBox.Show("NOTE: This key-combination assumes you are the survey proctor, and that the participant left. Press OK to quit the survey and to send it as is. Press Cancel to continue working.",
                    "Grit Survey: Proctor Override",
                    MessageBoxButtons.OKCancel);

                switch (dialogResult)
                {
                    case DialogResult.None:
                        break;
                    case DialogResult.OK:
                        survey.sectionDemographic = sectionDemographic;
                        survey.sectionGrit = sectionGrit;
                        survey.sectionImage = sectionImage;
                        survey.sectionNumber = sectionNumber;
                        survey.sectionWord = sectionWord;
                        //send survey to server w/ new method
                        sendSurvey();
                        Close();
                        break;
                    case DialogResult.Cancel:
                        break;
                    case DialogResult.Abort:
                        break;
                    case DialogResult.Retry:
                        break;
                    case DialogResult.Ignore:
                        break;
                    case DialogResult.Yes:
                        break;
                    case DialogResult.No:
                        break;
                    default:
                        break;
                }
            }
        }

        private void sendSurvey()
        {
            try
            {
                string s = JsonConvert.SerializeObject(survey);
                byte[] bytesToSend = Encoding.UTF8.GetBytes(s);
                Survey ss = JsonConvert.DeserializeObject<Survey>(s);
                TcpClient client = new TcpClient(Properties.Settings.Default.SERVER_ADDR, Properties.Settings.Default.SERVER_PORT);
                NetworkStream nwStream = client.GetStream();
                nwStream.Write(bytesToSend, 0, bytesToSend.Length);
                Thread.Sleep(2000);
                client.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.StackTrace);
            }
        }

        private void QuitSectionButton_Click(object sender, EventArgs e)
        {
            //Send answers to server if answer choice ends program
            DialogResult mBox = MessageBox.Show("Do you really wish to quit this section?",
                "Grit Survey",
                MessageBoxButtons.YesNo);
            switch (mBox)
            {
                case DialogResult.Abort:
                case DialogResult.Cancel:
                case DialogResult.Ignore:
                case DialogResult.No:
                    break;
                case DialogResult.Yes:
                    //Stop question timer and survey timer
                    if (questionNumber > -1)
                    {
                        saveAnswer(currentQuestion);
                    }
                    skippedQuestions.Clear();
                    clearAnswerChoices();
                    changeSection();
                    break;
                default:
                    break;
            }
        }

        public void answerChoice_Click(object sender, EventArgs e)
        {
            RadioButton radioButton = (RadioButton)sender;
            if (!radioButton.Checked)
            {
                Next_SkipButton.Text = "Next";
                clearAnswerChoices();
                radioButton.Checked = true;
                Next_SkipButton.Focus();
            }
            else if (radioButton.Checked)
            {
                radioButton.Checked = false;
                if (sectionId != 0)
                {
                    Next_SkipButton.Text = "Skip";
                }
            }
            else
            {
                if (!questionPanel.answerChoice0.Checked &&
                    !questionPanel.answerChoice1.Checked &&
                    !questionPanel.answerChoice2.Checked &&
                    !questionPanel.answerChoice3.Checked &&
                    !questionPanel.answerChoice4.Checked)
                {
                    Next_SkipButton.Text = "Next";
                }
                else
                {
                    Next_SkipButton.Text = "Skip";
                }
            }
        }

        private void clearAnswerChoices()
        {
            questionPanel.answerChoice0.Checked = false;
            questionPanel.answerChoice1.Checked = false;
            questionPanel.answerChoice2.Checked = false;
            questionPanel.answerChoice3.Checked = false;
            questionPanel.answerChoice4.Checked = false;
        }

        public void ageTextBox_Validating(object sender, CancelEventArgs e)
        {
            try
            {
                int age = int.Parse(demographicsPanel.ageTextBox.Text);
                if (age < 18 || age > 100)
                {
                    throw new Exception();
                }
                else
                {
                    ageErrorProvider.SetError(demographicsPanel.ageTextBox, "");
                }
            }
            catch (Exception)
            {
                ageErrorProvider.SetError(demographicsPanel.ageTextBox, "Invalid Age! Use a number between 18 and 100.");
                MessageBox.Show(ageErrorProvider.GetError(demographicsPanel.ageTextBox), "Grit Survey: Invalid Age");
            }
        }

        public void scoreTextBox_Validating(object sender, CancelEventArgs e)
        {
            try
            {
                int act = int.Parse(demographicsPanel.scoreTextBox.Text);
                if (demographicsPanel.scoreTypeCombobox.Text == "ACT Score")
                {
                    if (act < 1 || act > 36)
                    {
                        throw new Exception();
                    }
                    else
                    {
                        actErrorProvider.SetError(demographicsPanel.scoreTextBox, "");
                    }
                }
                else if (demographicsPanel.scoreTypeCombobox.Text == "SAT Score")
                {
                    if (act < 400 || act > 1600)
                    {
                        throw new Exception();
                    }
                    else
                    {
                        actErrorProvider.SetError(demographicsPanel.scoreTextBox, "");
                    }
                }
                else if (demographicsPanel.scoreTypeCombobox.Text == "Compass Score")
                {
                    if (act < 0 || act > 100)
                    {
                        throw new Exception();
                    }
                    else
                    {
                        actErrorProvider.SetError(demographicsPanel.scoreTextBox, "");
                    }
                }
                else if (demographicsPanel.scoreTypeCombobox.Text == "ASVAB")
                {
                    if (act < 1 || act > 99)
                    {
                        throw new Exception();
                    }
                    else
                    {
                        actErrorProvider.SetError(demographicsPanel.scoreTextBox, "");
                    }
                }

            }
            catch (Exception)
            {
                if (demographicsPanel.scoreTypeCombobox.Text == "ACT Score")
                {
                    actErrorProvider.SetError(demographicsPanel.scoreTextBox, "Invalid ACT Score! Use a number between 1 and 36.");
                    MessageBox.Show(actErrorProvider.GetError(demographicsPanel.scoreTextBox), "Grit Survey: Invalid ACT Score");
                }
                else if (demographicsPanel.scoreTypeCombobox.Text == "SAT Score")
                {
                    actErrorProvider.SetError(demographicsPanel.scoreTextBox, "Invalid SAT Score! Use a number between 400 and 1600.");
                    MessageBox.Show(actErrorProvider.GetError(demographicsPanel.scoreTextBox), "Grit Survey: Invalid SAT Score");
                }
                else if (demographicsPanel.scoreTypeCombobox.Text == "Compass Score")
                {
                    actErrorProvider.SetError(demographicsPanel.scoreTextBox, "Invalid Compass Score! Use a number between 0 and 100.");
                    MessageBox.Show(actErrorProvider.GetError(demographicsPanel.scoreTextBox), "Grit Survey: Invalid Compass Score");
                }
                else if (demographicsPanel.scoreTypeCombobox.Text == "ASVAB")
                {
                    actErrorProvider.SetError(demographicsPanel.scoreTextBox, "Invalid ASVAB Score! Use a number between 1 and 99.");
                    MessageBox.Show(actErrorProvider.GetError(demographicsPanel.scoreTextBox), "Grit Survey: Invalid ASVAB");
                }
            }
        }

        internal void GPAtextBox_Validating(object sender, CancelEventArgs e)
        {
            try
            {
                TextBox textBox = (TextBox)sender;
                float value = float.Parse(textBox.Text);

                if (value >= 0.0f && value <= 4.0f)
                {
                    gpaErrorProvider.SetError(demographicsPanel.GPAtextBox, "");
                }
                else
                {
                    throw new Exception();
                }
            }
            catch (Exception)
            {
                if (demographicsPanel.GPAtextBox.Text != "")
                {
                    gpaErrorProvider.SetError(demographicsPanel.GPAtextBox, "Invalid GPA! Use a number between 0.0 and 4.0");
                    MessageBox.Show(gpaErrorProvider.GetError(demographicsPanel.GPAtextBox), "Grit Survey: Invalid GPA");
                }
            }
        }

        private void InstructionsButton_Click(object sender, EventArgs e)
        {
            MessageBox.Show(instructions[sectionId], "Grit Survey: Instructions");
        }
    }
}
