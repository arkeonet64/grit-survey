﻿namespace Grit_Survey_Client
{
    partial class SurveyForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.titleTextBox = new System.Windows.Forms.TextBox();
            this.Next_SkipButton = new System.Windows.Forms.Button();
            this.QuitSectionButton = new System.Windows.Forms.Button();
            this.FinishTooltip = new System.Windows.Forms.ToolTip(this.components);
            this.QuitSectionTooltip = new System.Windows.Forms.ToolTip(this.components);
            this.ageErrorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            this.actErrorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            this.surveyPanel = new Grit_Survey_Client.SurveyPanel();
            this.InstructionsButton = new System.Windows.Forms.Button();
            this.InstructionsTooltip = new System.Windows.Forms.ToolTip(this.components);
            this.gpaErrorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.ageErrorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.actErrorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gpaErrorProvider)).BeginInit();
            this.SuspendLayout();
            // 
            // titleTextBox
            // 
            this.titleTextBox.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.titleTextBox.BackColor = System.Drawing.Color.White;
            this.titleTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.titleTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.titleTextBox.Location = new System.Drawing.Point(27, 13);
            this.titleTextBox.Name = "titleTextBox";
            this.titleTextBox.ReadOnly = true;
            this.titleTextBox.Size = new System.Drawing.Size(1147, 37);
            this.titleTextBox.TabIndex = 999;
            this.titleTextBox.TabStop = false;
            this.titleTextBox.Text = "Text";
            this.titleTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Next_SkipButton
            // 
            this.Next_SkipButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Next_SkipButton.Location = new System.Drawing.Point(1113, 565);
            this.Next_SkipButton.Name = "Next_SkipButton";
            this.Next_SkipButton.Size = new System.Drawing.Size(75, 23);
            this.Next_SkipButton.TabIndex = 0;
            this.Next_SkipButton.Text = "Next";
            this.FinishTooltip.SetToolTip(this.Next_SkipButton, "Sends the survey to the server and closes the window.");
            this.Next_SkipButton.UseVisualStyleBackColor = true;
            this.Next_SkipButton.Click += new System.EventHandler(this.nextButton_Click);
            // 
            // QuitSectionButton
            // 
            this.QuitSectionButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.QuitSectionButton.Location = new System.Drawing.Point(12, 565);
            this.QuitSectionButton.Name = "QuitSectionButton";
            this.QuitSectionButton.Size = new System.Drawing.Size(75, 23);
            this.QuitSectionButton.TabIndex = 2;
            this.QuitSectionButton.Text = "Quit Section";
            this.QuitSectionTooltip.SetToolTip(this.QuitSectionButton, "Advances the section and will save your data as unfinshed.");
            this.QuitSectionButton.UseVisualStyleBackColor = true;
            this.QuitSectionButton.Click += new System.EventHandler(this.QuitSectionButton_Click);
            // 
            // FinishTooltip
            // 
            this.FinishTooltip.Active = false;
            this.FinishTooltip.IsBalloon = true;
            this.FinishTooltip.ToolTipTitle = "Finsh Survey";
            // 
            // QuitSectionTooltip
            // 
            this.QuitSectionTooltip.IsBalloon = true;
            this.QuitSectionTooltip.ToolTipTitle = "Quit Section";
            // 
            // ageErrorProvider
            // 
            this.ageErrorProvider.ContainerControl = this;
            // 
            // actErrorProvider
            // 
            this.actErrorProvider.ContainerControl = this;
            // 
            // surveyPanel
            // 
            this.surveyPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.surveyPanel.AutoSize = true;
            this.surveyPanel.BackColor = System.Drawing.Color.White;
            this.surveyPanel.Location = new System.Drawing.Point(178, 72);
            this.surveyPanel.Name = "surveyPanel";
            this.surveyPanel.Size = new System.Drawing.Size(844, 487);
            this.surveyPanel.TabIndex = 13;
            // 
            // InstructionsButton
            // 
            this.InstructionsButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.InstructionsButton.Location = new System.Drawing.Point(1113, 536);
            this.InstructionsButton.Name = "InstructionsButton";
            this.InstructionsButton.Size = new System.Drawing.Size(75, 23);
            this.InstructionsButton.TabIndex = 14;
            this.InstructionsButton.Text = "Instructions";
            this.InstructionsTooltip.SetToolTip(this.InstructionsButton, "Opens a dialog to provide instructions for the current section.");
            this.InstructionsButton.UseVisualStyleBackColor = true;
            this.InstructionsButton.Click += new System.EventHandler(this.InstructionsButton_Click);
            // 
            // InstructionsTooltip
            // 
            this.InstructionsTooltip.IsBalloon = true;
            // 
            // gpaErrorProvider
            // 
            this.gpaErrorProvider.ContainerControl = this;
            // 
            // SurveyForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1200, 600);
            this.Controls.Add(this.InstructionsButton);
            this.Controls.Add(this.surveyPanel);
            this.Controls.Add(this.QuitSectionButton);
            this.Controls.Add(this.Next_SkipButton);
            this.Controls.Add(this.titleTextBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "SurveyForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "Grit Survey: Survey";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Shown += new System.EventHandler(this.GritSurveyQuestionnaire_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.ageErrorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.actErrorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gpaErrorProvider)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox titleTextBox;
        private System.Windows.Forms.Button Next_SkipButton;
        private System.Windows.Forms.Button QuitSectionButton;
        private System.Windows.Forms.ToolTip FinishTooltip;
        private System.Windows.Forms.ToolTip QuitSectionTooltip;
        private System.Windows.Forms.ErrorProvider ageErrorProvider;
        private System.Windows.Forms.ErrorProvider actErrorProvider;
        private SurveyPanel surveyPanel;
        private System.Windows.Forms.Button InstructionsButton;
        private System.Windows.Forms.ToolTip InstructionsTooltip;
        private System.Windows.Forms.ErrorProvider gpaErrorProvider;
    }
}