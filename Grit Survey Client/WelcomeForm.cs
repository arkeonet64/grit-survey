﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Grit_Survey_Client
{
    public partial class WelcomeForm : Form
    {
        public static string hostname = "";
        public WelcomeForm()
        {
            InitializeComponent();
            if (hostname != "")
                Properties.Settings.Default.SERVER_ADDR = hostname;
        }

        private void startSurveyButton_Click(object sender, EventArgs e)
        {
            SurveyForm surveyForm = new SurveyForm();
            surveyForm.ShowDialog();
        }
    }
}
